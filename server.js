#!/bin/env node
var express = require('express')
  , routes = require('./routes')
  , http = require('http')
  , upload = require('jquery-file-upload-middleware')
  , path = require('path');
var fs = require('fs'); 



// configure upload middleware
upload.configure({
    uploadDir: __dirname + '/public/images',
    uploadUrl: '/images',
    imageVersions: {
        thumbnail: {
            width: 80,
            height: 80
        }
    }
});

// mongodb
var Db = require('mongodb').Db;
var Server = require('mongodb').Server;

// Scope
  var self = this;

  // Setup
  self.dbServer = new mongodb.Server(process.env.OPENSHIFT_MONGODB_DB_HOST,parseInt(process.env.OPENSHIFT_MONGODB_DB_PORT));
  self.db = new mongodb.Db('nodews', self.dbServer, {auto_reconnect: true});
  self.dbUser = process.env.OPENSHIFT_MONGODB_DB_USERNAME;
  self.dbPass = process.env.OPENSHIFT_MONGODB_DB_PASSWORD;

var app = express();

app.configure(function(){
  app.locals.pretty = true;
  app.set('port', process.env.OPENSHIFT_NODEJS_PORT || 8080);
  app.set('ipaddress', process.env.OPENSHIFT_NODEJS_IP);
  app.set('views', __dirname + '/views');
  
  //app.set('view engine', 'jade');
  /*
  untuk akses pure html saja
  */
  app.engine('html', require('ejs').renderFile);
  //Favicon
// app.use(express.favicon( __dirname + "/public/favicon.ico", { maxAge: 2592000000 }));
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser('your secret here'));
  app.use(express.session());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
  //jika terjadi error maka akan redirect ke sini
  app.use(function(req,res){
    res.render('404.html');
});
  
});

app.configure('development', function(){
  app.use(express.errorHandler());
});


app.get('/', function(req,res){
	//res.render(routes.index); //khusus jade
	res.render("home.html");
});

app.post('/api/regiswisata/imageupload',function(req,res){
  console.log(req.body.imageupload);
  var cc = JSON.stringify(req.files);
  var kk = JSON.parse(cc);
   console.log(JSON.stringify(req.files));
     var temp_path = kk.files[0].path;
     var save_path = './public/images/hdpi/'+"mvkfvmfk"+kk.files[0].name
     console.log(kk);
     fs.rename(temp_path, save_path, function(error){
      if(error) throw error;
      
      fs.unlink(temp_path, function(){
        if(error) throw error;
        res.send("mvkfvmfk"+kk.files[0].name);
       //res.send("responsebroooo:"+req);
      });
      
     });  
})



app.get('/api/regiswisata/wisatalist',function(req,response){

  var otherObject  = [];
  var listData = function(err, collection) {
        collection.find().toArray(function(err, results) {
            //res.render('index.html', { layout : false , 'title' : 'Monode-crud', 'results' : results });
        otherObject  = JSON.stringify(results);
        console.log("Request handler 'random was called.");
        response.writeHead(200, {"Content-Type": "application/json"});
        response.write(otherObject);
        response.end();
  });
  }
 var Client = new Db('nodejsgis', new Server('127.0.0.1', 27017, {}));
    Client.open(function(err, pClient) {
        Client.collection('tbl_wisata', listData);
    });   
});

app.post('/api/regiswisata/addwisata',function(req,res){
  console.log(req.body);
  console.log(req.body.userPhotoInput);
  var cc = JSON.stringify(req.files);
  var kk = JSON.parse(cc);
 console.log(kk.files[0].name);
  var data = {
    'nama_wisata' : req.body.nama_wisata , 
    'daerah_wisata' : req.body.name_daerah,
    'userPhotoInput' : "mvkfvmfk"+kk.files[0].name,
    'latitude' : req.body.latitude,
    'longtitude' : req.body.longtitude,
    'tag_deskripsi' : req.body.tag_deskripsi,
    'deskripsi' : req.body.deskripsi };

    var insertData = function(err, collection) {
        collection.insert(data);
    }

    var Client = new Db('nodejsgis', new Server('127.0.0.1', 27017, {}));
    Client.open(function(err, pClient) {
        Client.collection('tbl_wisata', insertData);
        Client.close();
    });

  res.send(req.body);
});


app.get('/signup',function(req,res){
	res.render('testsignup.html');
});

app.get('/regiswisata',function(req,res){
	//res.render('regiswisata.html');
  var listData = function(err, collection) {
        collection.find().toArray(function(err, results) {
            res.render('regiswisata.html', { layout : false , 'title' : 'Add Wisata', 'results' : results });
        });
    }
    var Client = new Db('nodejsgis', new Server('127.0.0.1', 27017, {}));
    Client.open(function(err, pClient) {
        Client.collection('wisata', listData);
        //Client.close();
    });
});



http.createServer(app).listen(app.get('port'), app.get('ipaddress'), function(){
  console.log("Express server listening on port " + app.get('port'));
});


